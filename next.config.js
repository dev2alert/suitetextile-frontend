module.exports = {
    compiler: {
        styledComponents: true
    },
    images: {
        domains: ["api.suitetextile.ru"]
    }
};
