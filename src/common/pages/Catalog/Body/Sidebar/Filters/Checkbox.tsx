import React from "react";
import Image from "next/image";
import styled from "styled-components";
import {Text} from "@common/Text";
import checkboxIcon from "./assets/checkbox-icon.svg";

export const SidebarFiltersCheckboxStyled = styled.label`
    display: inline-flex;
    gap: 8px;
    align-items: center;
    cursor: pointer;
`;

export const SidebarFiltersCheckboxInputContainer = styled.span`
    display: inline-flex;
`;

export const SidebarFiltersCheckboxInputNative = styled.input`
    display: none;
`;

export const SidebarFiltersCheckboxIcon = styled(Image)`
    display: inline-flex;
`;

export const SidebarFiltersCheckboxInput = styled.span`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 24px;
    height: 24px;
    border: 2px solid rgba(36, 30, 12, 0.2);
    border-radius: 4px;
    ${SidebarFiltersCheckboxInputNative}:checked + & {
        border: none;
        background: ${({theme}) => theme.palette.black};
    }
    ${SidebarFiltersCheckboxInputNative}:not(:checked) + & {
        > ${SidebarFiltersCheckboxIcon} {
            display: none;
        }
    }
`;

export const SidebarFiltersCheckboxCount = styled.sup`
    font: normal 9px ${({theme}) => theme.fonts.roboto};
    color: ${({theme}) => theme.palette.black};
    margin-left: 2px;
`;

export const SidebarFiltersCheckboxLabel = styled(Text)``;

export interface SidebarFiltersCheckboxProps extends React.PropsWithChildren {
    count?: number;
    checked?: boolean;
    onChange?: () => unknown;
}

export const SidebarFiltersCheckbox: React.FC<SidebarFiltersCheckboxProps> = ({
    count,
    checked,
    children,
    onChange
}) => (
    <SidebarFiltersCheckboxStyled>
        <SidebarFiltersCheckboxInputContainer>
            <SidebarFiltersCheckboxInputNative
                type="checkbox"
                checked={checked}
                onChange={onChange}
            />
            <SidebarFiltersCheckboxInput>
                <SidebarFiltersCheckboxIcon
                    src={checkboxIcon}
                    width={12}
                    height={10}
                    alt="Иконка галочки для фильтра"
                    priority
                />
            </SidebarFiltersCheckboxInput>
        </SidebarFiltersCheckboxInputContainer>
        <SidebarFiltersCheckboxLabel>
            {children}
            {count ? <SidebarFiltersCheckboxCount>{count}</SidebarFiltersCheckboxCount> : null}
        </SidebarFiltersCheckboxLabel>
    </SidebarFiltersCheckboxStyled>
);
