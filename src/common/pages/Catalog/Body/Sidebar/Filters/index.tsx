import React, {useCallback, useEffect, useState} from "react";
import styled from "styled-components";
import {motion} from "framer-motion";
import {produce} from "immer";
import {Button} from "@common/Button";
import {CircularProgress} from "@common/CircularProgress";
import {
    MobileMenu,
    MobileMenuDrawerFooter,
    MobileMenuDrawerFooterButtons
} from "@common/MobileMenu";
import {MobileMenuDesktopElement, MobileMenuElement} from "@common/MobileMenu/Element";
import {SaleFilter, SaleFilterOption, SaleFilterSelected, SaleFiltersSelected} from "@common/api";
import {useCatalog} from "@common/pages/Catalog";
import {SidebarFiltersCheckbox} from "./Checkbox";
import {SidebarFiltersGroup} from "./Group";

export const SidebarDesktopFilters = styled(MobileMenuDesktopElement)`
    display: flex;
    flex-direction: column;
    gap: 30px;
    align-items: flex-start;
`;

export const SidebarDesktopFiltersLoading = styled(MobileMenuDesktopElement)`
    display: flex;
    width: 100%;
    height: calc(100vh - ${({theme}) => theme.header.height} - 200px);
    justify-content: center;
    align-items: center;
    cursor: progress;
`;

export const SidebarMobileFilters = styled(MobileMenuElement)`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
`;

export const SidebarFilters: React.FC = () => {
    const {
        saleFilters,
        filtersMenuOpened,
        filtersSelected,
        setFiltersSelected,
        closeFiltersMenu,
        isLoading,
        isFirstLoading
    } = useCatalog();

    const handleFiltersCheckboxChange = useCallback(
        (
            filter: SaleFilter,
            option: SaleFilterOption,
            filtersSelected: SaleFiltersSelected,
            setFiltersSelected: (filtersSelected: SaleFiltersSelected) => void
        ) => {
            setFiltersSelected(
                produce(filtersSelected, (filtersSelected) => {
                    const filterSelected: SaleFilterSelected = filtersSelected[filter.code] ?? {
                        id: filter.id,
                        code: filter.code,
                        name: filter.name,
                        options: {}
                    };

                    if (option.id in filterSelected.options) {
                        delete filterSelected.options[option.id];
                    } else {
                        const optionSelected: SaleFilterOption = {
                            id: option.id,
                            name: option.name,
                            code: option.code,
                            count: option.count
                        };

                        filterSelected.options[option.id] = optionSelected;
                    }

                    filtersSelected[filter.code] = filterSelected;
                })
            );
        },
        []
    );

    const [mobileFiltersSelected, setMobileFiltersSelected] = useState<SaleFiltersSelected>({});

    const resetMobileFiltersSelected = useCallback(() => {
        setMobileFiltersSelected(
            produce((mobileFiltersSelected) => {
                for (const code in mobileFiltersSelected) {
                    mobileFiltersSelected[code] = {
                        ...mobileFiltersSelected[code],
                        options: {}
                    };
                }
            })
        );
    }, [setMobileFiltersSelected]);

    useEffect(() => {
        setMobileFiltersSelected(filtersSelected);
    }, [setMobileFiltersSelected, filtersSelected]);

    const handleMobileFiltersSelectedApply = useCallback(() => {
        setFiltersSelected(mobileFiltersSelected);
        closeFiltersMenu();
    }, [setFiltersSelected, mobileFiltersSelected, closeFiltersMenu]);

    if (isFirstLoading) {
        return (
            <SidebarDesktopFiltersLoading>
                <CircularProgress />
            </SidebarDesktopFiltersLoading>
        );
    }

    return (
        <>
            <SidebarDesktopFilters as={motion.div} animate={{opacity: isLoading ? 0.4 : 1}}>
                {saleFilters.map((filter) => {
                    return (
                        <SidebarFiltersGroup key={filter.id} label={filter.name}>
                            {filter.options.map((option) => {
                                return (
                                    <SidebarFiltersCheckbox
                                        key={option.id}
                                        checked={!!filtersSelected[filter.code]?.options[option.id]}
                                        count={option.count}
                                        onChange={handleFiltersCheckboxChange.bind(
                                            null,
                                            filter,
                                            option,
                                            filtersSelected,
                                            setFiltersSelected
                                        )}
                                    >
                                        {option.name}
                                    </SidebarFiltersCheckbox>
                                );
                            })}
                        </SidebarFiltersGroup>
                    );
                })}
            </SidebarDesktopFilters>
            <MobileMenu
                label="Фильтры"
                open={filtersMenuOpened}
                footer={
                    <MobileMenuDrawerFooter>
                        <MobileMenuDrawerFooterButtons>
                            <Button
                                variant="outlined"
                                fullWidth
                                onClick={resetMobileFiltersSelected}
                            >
                                Сбросить
                            </Button>
                            <Button fullWidth onClick={handleMobileFiltersSelectedApply}>
                                Применить
                            </Button>
                        </MobileMenuDrawerFooterButtons>
                    </MobileMenuDrawerFooter>
                }
                onClose={closeFiltersMenu}
            >
                <SidebarMobileFilters as="div">
                    {saleFilters.map((filter) => {
                        return (
                            <SidebarFiltersGroup key={filter.id} label={filter.name}>
                                {filter.options.map((option) => {
                                    return (
                                        <SidebarFiltersCheckbox
                                            key={option.id}
                                            checked={
                                                !!mobileFiltersSelected[filter.code]?.options[
                                                    option.id
                                                ]
                                            }
                                            onChange={handleFiltersCheckboxChange.bind(
                                                null,
                                                filter,
                                                option,
                                                mobileFiltersSelected,
                                                setMobileFiltersSelected
                                            )}
                                        >
                                            {option.name}
                                        </SidebarFiltersCheckbox>
                                    );
                                })}
                            </SidebarFiltersGroup>
                        );
                    })}
                </SidebarMobileFilters>
            </MobileMenu>
        </>
    );
};
