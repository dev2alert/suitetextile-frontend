import React, {useCallback, useEffect, useMemo, useState} from "react";
import {useRouter} from "next/router";
import {Page} from "@common/Page";
import {
    CatalogListBrandOption,
    CatalogListDefaultOption,
    CatalogListFoldersOption,
    GetBrandCategoriesResult,
    GetSaleItemsOptions,
    SaleFilter,
    SaleFilterSelected,
    SaleItemsFilters,
    useApi,
    useSaleItemsQuery
} from "@common/api";
import {GetSaleItemsResult} from "@common/api";
import {AppPageProps, useApp} from "@common/app";
import {BrandPage} from "../Brand";
import {SeriaPage} from "../Seria";
import {CatalogBody} from "./Body";
import {CatalogProvider} from "./Context";
import {CatalogHead} from "./Head";
import {CatalogPath, getCatalogPath} from "./Path";
import {useCatalogQueryBuilder} from "./QueryBuilder";
import {CatalogSection} from "./Section";

export interface CatalogPageProps {
    brandCategories?: GetBrandCategoriesResult | null;
    saleItems?: GetSaleItemsResult;
}

export const CatalogPage: React.FC<AppPageProps & CatalogPageProps> = ({
    brandCategories = null,
    saleItems: initialSaleItems
}) => {
    const {catalogLists} = useApp();
    const router = useRouter();
    const {name, subname} = router.query;
    const api = useApi();
    const category = useMemo<CatalogListFoldersOption | null>(() => {
        const catalogPath: CatalogPath = getCatalogPath(router.query);
        let category: CatalogListFoldersOption | null = null;

        category = api.catalog.utils.getCategoryByUrl({
            url: catalogPath.toString(),
            catalogLists
        });
        if (router.query.category) {
            category ??= api.catalog.utils.getCategoryById({
                id: +router.query.category,
                catalogLists
            });
        }

        return category;
    }, [catalogLists, router.query]);
    const parentCategory = useMemo<CatalogListFoldersOption | null>(() => {
        if (category !== null) {
            return api.catalog.utils.getParentCategory({
                catalogLists,
                category
            });
        } else {
            return null;
        }
    }, [category]);
    const catalogCategories = useMemo<CatalogListFoldersOption[]>(() => {
        if (category === null) {
            return catalogLists.categories.options.filter((option) => option._type === 1842);
        } else {
            return catalogLists.categories.options.filter(
                (option) => option.parent_id === category?.id
            );
        }
    }, [catalogLists, category]);
    const [brand, seria] = useMemo<
        [CatalogListBrandOption | null, CatalogListDefaultOption | null]
    >(() => {
        let brand: CatalogListBrandOption | null = null;
        let seria: CatalogListDefaultOption | null = null;

        if (typeof name === "string" && typeof subname === "string") {
            brand = api.catalog.utils.getBrandByCode({code: name, catalogLists});
            seria = api.catalog.utils.getSeriaByCode({code: subname, catalogLists});
        } else if (typeof name === "string") {
            brand = api.catalog.utils.getBrandByCode({code: name, catalogLists});
        }

        return [brand, seria];
    }, [name, subname, catalogLists]);
    const {catalogQueryBuilder, sort, setSort, filtersSelected, setFiltersSelected, resetFilters} =
        useCatalogQueryBuilder({router, catalogLists});
    const saleItemsQueryOptions = useMemo<GetSaleItemsOptions>(
        () => catalogQueryBuilder.toGetSaleItemsOptions({category, filtersSelected}),
        [catalogQueryBuilder, category]
    );
    const saleItemsQuery = useSaleItemsQuery({
        initialSaleItems,
        brand: brand?.id,
        seria: seria?.id,
        ...saleItemsQueryOptions
    });

    const [filtersMenuOpened, setFiltersMenuOpened] = useState(false);
    const openFiltersMenu = useCallback(() => {
        setFiltersMenuOpened(true);
    }, []);
    const closeFiltersMenu = useCallback(() => {
        setFiltersMenuOpened(false);
    }, []);

    const saleFilters = useMemo<SaleFilter[]>(() => {
        const saleItemsFilters: SaleItemsFilters | null =
            saleItemsQuery.data?.pages[0]?.filters ?? null;

        if (category !== null && saleItemsFilters !== null) {
            return api.catalog.utils.getSaleFilters({catalogLists, saleItemsFilters});
        } else {
            return [];
        }
    }, [api, catalogLists, saleItemsQuery]);

    const catalogName = useMemo<string>(() => {
        if (!category) {
            return "Элитный текстиль для дома";
        } else {
            let catalogName = "";
            const filterListSelected: SaleFilterSelected[] = Object.values(filtersSelected);
            if (filterListSelected.length > 0) {
                catalogName +=
                    filterListSelected
                        .map((filter) =>
                            Object.values(filter.options)
                                .map((option) => option.name)
                                .join(" / ")
                        )
                        .join(" / ") + " / ";
            }
            catalogName += category.pagetitle ? category.pagetitle : category.name;

            return catalogName;
        }
    }, [category, filtersSelected]);

    const [isFirstSuccess, setIsFirstSuccess] = useState(false);
    useEffect(() => {
        if (saleItemsQuery.isSuccess && !saleItemsQuery.isFetching) {
            setIsFirstSuccess(true);
        }
    }, [saleItemsQuery.isSuccess, saleItemsQuery.isFetching]);
    const isFirstLoading: boolean =
        !initialSaleItems && saleItemsQuery.isFetching && saleItemsQuery.isInitialLoading;
    const isLoading: boolean = isFirstSuccess && saleItemsQuery.isFetching;

    let pageNode: React.ReactNode;
    if (brand !== null && seria !== null) {
        pageNode = <SeriaPage seria={seria} brand={brand} />;
    } else if (brand !== null && brandCategories !== null) {
        pageNode = <BrandPage brand={brand} brandCategories={brandCategories} />;
    } else {
        pageNode = (
            <Page title={catalogName}>
                <CatalogSection>
                    <CatalogHead />
                    <CatalogBody />
                </CatalogSection>
            </Page>
        );
    }

    return (
        <CatalogProvider
            catalogLists={catalogLists}
            saleItemsQuery={saleItemsQuery}
            category={category}
            saleFilters={saleFilters}
            sort={sort}
            setSort={setSort}
            filtersSelected={filtersSelected}
            setFiltersSelected={setFiltersSelected}
            resetFilters={resetFilters}
            filtersMenuOpened={filtersMenuOpened}
            openFiltersMenu={openFiltersMenu}
            closeFiltersMenu={closeFiltersMenu}
            catalogCategories={catalogCategories}
            parentCategory={parentCategory}
            isFirstSuccess={isFirstSuccess}
            isFirstLoading={isFirstLoading}
            isLoading={isLoading}
        >
            {pageNode}
        </CatalogProvider>
    );
};

export * from "./Context";
export * from "./getServerSideProps";
