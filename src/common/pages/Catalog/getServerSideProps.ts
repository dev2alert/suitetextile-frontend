import {
    CatalogListBrandOption,
    CatalogListDefaultOption,
    CatalogListFoldersOption,
    GetBrandCategoriesResult,
    GetSaleItemsOptions,
    GetSaleItemsResult,
    SaleFiltersSelected
} from "@common/api";
import {GetAppPageProps, getAppPageProps} from "@common/app";
import {CatalogPageProps} from "@common/pages/Catalog";
import {CatalogPath, getCatalogPath} from "./Path";
import {CatalogQueryBuilder} from "./QueryBuilder";

export type GetCatalogServerSideProps = GetAppPageProps<CatalogPageProps>;

export const getCatalogServerSideProps: GetCatalogServerSideProps =
    getAppPageProps<CatalogPageProps>(async ({api, query, catalogLists, isPushRequest}) => {
        const catalogPath: CatalogPath = getCatalogPath(query);
        const {name, subname} = catalogPath;

        if (typeof name === "string" && subname === "index.phtml") {
            catalogPath.subname = null;

            return {
                redirect: {
                    statusCode: 301,
                    destination: catalogPath.toString()
                }
            };
        }

        let category: CatalogListFoldersOption | null = null;
        let brand: CatalogListBrandOption | null = null;
        let brandCategories: GetBrandCategoriesResult | null = null;
        let seria: CatalogListDefaultOption | null = null;

        category = api.catalog.utils.getCategoryByUrl({
            url: catalogPath.toString(),
            catalogLists
        });
        if (query.category) {
            category ??= api.catalog.utils.getCategoryById({
                id: +query.category,
                catalogLists
            });
        }
        if (name !== null && subname !== null) {
            brand = api.catalog.utils.getBrandByCode({code: name, catalogLists});
            seria = api.catalog.utils.getSeriaByCode({code: subname, catalogLists});
        } else if (name !== null) {
            brand = api.catalog.utils.getBrandByCode({code: name, catalogLists});
            if (brand !== null) {
                const {id} = brand;

                brandCategories = await api.catalog.getBrandCategories({id});
            }
        }

        if (name !== null && brand === null && category === null) {
            return {
                notFound: true
            };
        }
        if (isPushRequest) {
            return {
                props: {
                    brandCategories
                }
            };
        }

        const catalogQueryBuilder = new CatalogQueryBuilder(query);
        const filtersSelected: SaleFiltersSelected = catalogQueryBuilder.getFiltersSelected({
            catalogLists
        });
        const saleItemsQueryOptions: GetSaleItemsOptions =
            catalogQueryBuilder.toGetSaleItemsOptions({category, filtersSelected});
        const saleItems: GetSaleItemsResult = await api.catalog.getSaleItems({
            brand: brand?.id,
            seria: seria?.id,
            ...saleItemsQueryOptions
        });

        return {
            props: {
                brandCategories,
                saleItems
            }
        };
    });
