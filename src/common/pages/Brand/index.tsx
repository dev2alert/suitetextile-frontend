import React, {useMemo} from "react";
import styled from "styled-components";
import {Container} from "@common/Container";
import {Page} from "@common/Page";
import {Section} from "@common/Section";
import {
    CatalogListBrandOption,
    CatalogListDefaultOption,
    GetBrandCategoriesResult,
    useApi
} from "@common/api";
import {useCatalog} from "../Catalog";
import {BrandProvider} from "./Context";
import {BrandInfo} from "./Info";
import {Main} from "./Main";
import {MobileCategories} from "./MobileCategories";

export const BrandSection = styled(Section)`
    padding: 30px 0px;
    > ${Container} {
        display: flex;
        align-items: flex-start;
        gap: 30px;
        @media (max-width: 1100px) {
            flex-direction: column;
            align-items: center;
        }
    }
`;

export interface BrandPageProps {
    brand: CatalogListBrandOption;
    brandCategories: GetBrandCategoriesResult;
}

export const BrandPage: React.FC<BrandPageProps> = ({brand, brandCategories}) => {
    const {saleItemsQuery, catalogLists} = useCatalog();
    const api = useApi();
    const brandName: string = brand.name.replace(/\\/g, "");
    const stars: number | null = brand.stars ? +brand.stars : null;
    const sold: number | null = brand.sold ? +brand.sold : null;
    const series = useMemo<CatalogListDefaultOption[]>(
        () => api.catalog.utils.getSeriesByBrand({catalogLists, brand}),
        [api, catalogLists, brand]
    );

    return (
        <BrandProvider
            brand={brand}
            brandName={brandName}
            stars={stars}
            sold={sold}
            series={series}
            brandCategories={brandCategories}
            saleItemsQuery={saleItemsQuery}
            catalogLists={catalogLists}
        >
            <Page title={"Бренд " + brandName}>
                <BrandSection>
                    <MobileCategories />
                    <BrandInfo />
                    <Main />
                </BrandSection>
            </Page>
        </BrandProvider>
    );
};

export * from "./Context";
