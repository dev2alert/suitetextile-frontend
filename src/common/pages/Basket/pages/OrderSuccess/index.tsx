import React, {useEffect, useMemo} from "react";
import styled from "styled-components";
import {useBasket} from "@common/Basket";
import {Page} from "@common/Page";
import {Section} from "@common/Section";
import {Text} from "@common/Text";
import {Title} from "@common/Title";
import {GetOrderSuccessResult, useApi} from "@common/api";

export const OrderSuccessSection = styled(Section)`
    padding-top: 40px;
    padding-bottom: 40px;
`;

export const OrderSuccessTitle = styled(Title)`
    font-size: 32px;
`;

export const OrderSuccessInfo = styled.div`
    display: flex;
`;

export const OrderSuccessInfoList = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-top: 30px;
`;

export const OrderSuccessInfoItem = styled(Text)`
    display: flex;
    font-size: 16px;
    line-height: 1.5;
`;

export interface OrderSuccessPageProps {
    orderSuccess: GetOrderSuccessResult;
}

export const OrderSuccessPage: React.FC<OrderSuccessPageProps> = ({orderSuccess}) => {
    const api = useApi();
    const {basketContentsQuery} = useBasket();

    const {order} = orderSuccess;
    if (order === false) {
        return null;
    }

    const fio = useMemo<string | null>(() => api.user.utils.getFio(order.user), [api, order.user]);
    const address = useMemo<string | null>(() => {
        if (order.delivery.address) {
            return order.delivery.address;
        } else {
            return null;
        }
    }, [order.delivery.address]);
    const phone = useMemo<string | null>(() => {
        if (order.user.phone) {
            return order.user.phone;
        } else {
            return null;
        }
    }, [order.user.phone]);
    const email = useMemo<string | null>(() => {
        if (order.user.email) {
            return order.user.email;
        } else {
            return null;
        }
    }, [order.user.email]);
    const totalPrice = useMemo<string>(
        () => api.catalog.utils.formatPrice(order.items.sum),
        [api, order.items.sum]
    );

    useEffect(() => {
        basketContentsQuery.refetch();
    }, []);

    return (
        <Page title="Успешный заказ">
            <OrderSuccessSection fullHeight>
                <OrderSuccessTitle>Успешный заказ</OrderSuccessTitle>
                <OrderSuccessInfo>
                    <OrderSuccessInfoList>
                        {fio ? <OrderSuccessInfoItem>ФИО: {fio}</OrderSuccessInfoItem> : null}
                        {address !== null ? (
                            <OrderSuccessInfoItem>Адрес: {address}</OrderSuccessInfoItem>
                        ) : null}
                        {phone !== null ? (
                            <OrderSuccessInfoItem>Телефон: {phone}</OrderSuccessInfoItem>
                        ) : null}
                        {email !== null ? (
                            <OrderSuccessInfoItem>E-mail: {email}</OrderSuccessInfoItem>
                        ) : null}
                        <OrderSuccessInfoItem>Сумма: {totalPrice}</OrderSuccessInfoItem>
                    </OrderSuccessInfoList>
                </OrderSuccessInfo>
            </OrderSuccessSection>
        </Page>
    );
};
