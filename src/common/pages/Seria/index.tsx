import React from "react";
import styled from "styled-components";
import {Container} from "@common/Container";
import {Page} from "@common/Page";
import {Section} from "@common/Section";
import {CatalogListBrandOption, CatalogListDefaultOption} from "@common/api";
import {useCatalog} from "../Catalog";
import {SeriaProvider} from "./Context";
import {SeriaInfo} from "./Info";
import {Main} from "./Main";

export const SeriaSection = styled(Section)`
    padding: 30px 0px;
    > ${Container} {
        display: flex;
        align-items: flex-start;
        gap: 30px;
        @media (max-width: 1100px) {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    }
`;

export interface SeriaPageProps {
    seria: CatalogListDefaultOption;
    brand: CatalogListBrandOption;
}

export const SeriaPage: React.FC<SeriaPageProps> = ({seria, brand}) => {
    const {saleItemsQuery, catalogLists} = useCatalog();
    const brandName: string = brand.name.replace(/\\/g, "");
    const stars: number | null = brand.stars ? +brand.stars : null;
    const sold: number | null = brand.sold ? +brand.sold : null;

    return (
        <SeriaProvider
            seria={seria}
            brand={brand}
            brandName={brandName}
            stars={stars}
            sold={sold}
            saleItemsQuery={saleItemsQuery}
            catalogLists={catalogLists}
        >
            <Page title={"Коллекция " + seria.name}>
                <SeriaSection>
                    <SeriaInfo />
                    <Main />
                </SeriaSection>
            </Page>
        </SeriaProvider>
    );
};
