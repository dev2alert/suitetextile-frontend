import React, {useCallback, useContext} from "react";
import {useToast} from "@common/Toast";
import {
    AddBasketBulkFailureResult,
    AddBasketBulkMutation,
    BasketContentsQuery,
    DeleteBasketItemMutation,
    DeleteBasketItemOptions,
    SaleOption,
    UseBasketContentsQueryOptions,
    useAddBasketBulkMutation,
    useBasketContentsQuery,
    useDeleteBasketItemMutation
} from "@common/api";

export interface IBasketContext {
    basketContentsQuery: BasketContentsQuery;
    addBasketBulkMutation: AddBasketBulkMutation;
    deleteBasketItemMutation: DeleteBasketItemMutation;
    addSaleItemToBasket: (optionSelected: SaleOption) => Promise<void>;
    deleteBasketItem: (options: DeleteBasketItemOptions) => void;
}

export const BasketContext = React.createContext<IBasketContext | null>(null);

export const BasketProvider: React.FC<React.PropsWithChildren & UseBasketContentsQueryOptions> = ({
    children,
    ...options
}) => {
    const toast = useToast();
    const basketContentsQuery = useBasketContentsQuery(options);
    const addBasketBulkMutation = useAddBasketBulkMutation({basketContentsQuery});
    const deleteBasketItemMutation = useDeleteBasketItemMutation({basketContentsQuery});

    const addSaleItemToBasket = useCallback(
        async (optionSelected: SaleOption) => {
            const colors: number[] = optionSelected.color?.map(Number) ?? [];
            const sizes: number[] = [optionSelected.id];

            const result: AddBasketBulkFailureResult = await addBasketBulkMutation.mutateAsync({
                colors,
                sizes
            });

            if (result.result === "failure") {
                toast.show({
                    type: "error",
                    message: result.reason,
                    position: "bottom-center"
                });
            } else if (result.result === "success") {
                toast.show({
                    type: "success",
                    message: "Товар добавлен в корзину",
                    position: "bottom-center"
                });
            }
        },
        [toast]
    );

    const deleteBasketItem = useCallback(async (options: DeleteBasketItemOptions) => {
        await deleteBasketItemMutation.mutate(options);

        toast.show({
            type: "success",
            message: "Товар удален из корзины",
            position: "bottom-center"
        });
    }, []);

    return (
        <BasketContext.Provider
            value={{
                basketContentsQuery,
                addBasketBulkMutation,
                deleteBasketItemMutation,
                addSaleItemToBasket,
                deleteBasketItem
            }}
        >
            {children}
        </BasketContext.Provider>
    );
};

export function useBasket(): IBasketContext {
    const context = useContext(BasketContext);

    if (!context) {
        throw new Error("useBasket must be used within a BasketProvider.");
    }

    return context;
}
