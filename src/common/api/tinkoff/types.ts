export interface OrderPaymentOptions {
    orderId: number;
}

export interface OrderPaymentInfo {
    id: string;
    url: string;
}

export interface OrderPaymentSuccessResult {
    result: "success";
    payment: OrderPaymentInfo;
}

export interface OrderPaymentErrorResult {
    result: "error";
    reason: string;
}

export type OrderPaymentResult = OrderPaymentSuccessResult | OrderPaymentErrorResult;
