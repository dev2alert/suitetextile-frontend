import {SuiteTextileApi} from "..";
import {OrderPaymentOptions, OrderPaymentResult} from "./types";

export class TinkoffApi {
    constructor(private readonly api: SuiteTextileApi) {}

    public async orderPayment({orderId}: OrderPaymentOptions): Promise<OrderPaymentResult> {
        const {data: result} = await this.api.instance.get<OrderPaymentResult>(
            "tinkoff/order-payment",
            {params: {id: orderId}}
        );

        return result;
    }
}
