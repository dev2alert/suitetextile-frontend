import {UseMutationResult, useMutation} from "@tanstack/react-query";
import {
    BasketContentsQuery,
    DeleteBasketItemOptions,
    DeleteBasketItemResult,
    useApi
} from "@common/api";

export interface UseDeleteBasketItemMutationOptions {
    basketContentsQuery: BasketContentsQuery;
}

export type DeleteBasketItemMutation = UseMutationResult<
    DeleteBasketItemResult,
    unknown,
    DeleteBasketItemOptions
>;

export const useDeleteBasketItemMutation = ({
    basketContentsQuery
}: UseDeleteBasketItemMutationOptions): DeleteBasketItemMutation => {
    const api = useApi();

    return useMutation<DeleteBasketItemResult, unknown, DeleteBasketItemOptions>(
        ["deleteBasketItem"],
        (options) => api.basket.deleteItem(options),
        {
            onSuccess: () => {
                basketContentsQuery.refetch();
            }
        }
    );
};
