import {UseMutationResult, useMutation} from "@tanstack/react-query";
import {AddBasketBulkOptions, AddBasketBulkResult, BasketContentsQuery, useApi} from "@common/api";

export interface UseAddBasketBulkMutationOptions {
    basketContentsQuery: BasketContentsQuery;
}

export type AddBasketBulkMutation = UseMutationResult<
    AddBasketBulkResult,
    unknown,
    AddBasketBulkOptions
>;

export const useAddBasketBulkMutation = ({
    basketContentsQuery
}: UseAddBasketBulkMutationOptions): AddBasketBulkMutation => {
    const api = useApi();

    return useMutation<AddBasketBulkResult, unknown, AddBasketBulkOptions>(
        ["addBasketBulk"],
        (options) => api.basket.addBulk(options),
        {
            onSuccess: () => {
                basketContentsQuery.refetch();
            }
        }
    );
};
