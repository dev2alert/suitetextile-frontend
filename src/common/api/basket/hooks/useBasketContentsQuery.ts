import {UseQueryResult, useQuery} from "@tanstack/react-query";
import {GetBasketContentsResult, useApi} from "@common/api";

export interface UseBasketContentsQueryOptions {
    initialBasketContents?: GetBasketContentsResult | null;
}

export type BasketContentsQuery = UseQueryResult<GetBasketContentsResult>;

export const useBasketContentsQuery = ({
    initialBasketContents
}: UseBasketContentsQueryOptions = {}): BasketContentsQuery => {
    const api = useApi();

    return useQuery(["basketContents"], () => api.basket.getContents(), {
        initialData: initialBasketContents,
        cacheTime: 0
    });
};
