export interface SaleOption {
    id: number;
    seria: string;
    color: string[] | null;
    size: string;
    fas: string;
    name: string;
    price: string;
    newprice: string;
    picture: string;
    photo370: string;
    photo56: string;
    present: boolean;
    komp: string;
    degree: string;
    sheettype: string;
    ts: string;
    weight: string;
    types: string;
    komplekt: string;
    quantity: string;
    sex: string;
}

export interface SaleImage {
    id: number;
    color: string[];
    name: string;
    picture: string;
    pic: string;
    photo370: string;
    photo56: string;
    fas: string[];
}

export interface SaleListItem {
    id: number;
    name: string;
    url: string;
    parent_id: number;
    stars: string;
    sold: string;
    images: SaleImage[];
    options: SaleOption[] | boolean;
    brand_id: string;
    shades_id: string;
    picture: string;
    text: string;
    brand: string;
    seria: string;
    types: string;
    material: string;
    shades: string;
    color: string;
    country: string;
    plotnost: string;
    size: string;
    fas: string;
    compliance: string;
    pagetitle: string;
    structure: string;
}

export type SaleItemsFilters = Record<string, Record<string, number>>;

export interface GetSaleItemsResult {
    result: string;
    items: SaleListItem[];
    filters: SaleItemsFilters;
    start: number;
    perPage: number;
    total: number;
}

export enum SaleSort {
    CHEAPER = "#min_price",
    EXPENSIVE = "-#min_price",
    NAME = "_name",
    NEW = "-_create_time"
}

export type SaleGoods = "all" | number;

export interface GetSaleItemsOptions {
    goods?: SaleGoods;
    start?: number;
    brand?: number;
    seria?: number;
    sort?: SaleSort;
    filters?: Record<string, string>;
}

export type SaleItem = SaleListItem;

export interface GetSaleItemOptions {
    path: string;
}

export interface GetSaleItemResult {
    result: string;
    item: SaleItem | false;
}
