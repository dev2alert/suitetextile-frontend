import {OrderSuccessPage} from "@common/pages/Basket/pages/OrderSuccess";
import {
    GetOrderSuccessServerSideProps,
    getOrderSuccessServerSideProps
} from "@common/pages/Basket/pages/OrderSuccess/getServerSideProps";

export const getServerSideProps: GetOrderSuccessServerSideProps = getOrderSuccessServerSideProps;

export default OrderSuccessPage;
