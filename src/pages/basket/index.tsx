import {GetAppPageProps, getAppPageProps} from "@common/app";
import {BasketPage} from "@common/pages/Basket";

export const getServerSideProps: GetAppPageProps = getAppPageProps();

export default BasketPage;
