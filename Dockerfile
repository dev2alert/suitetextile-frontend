FROM node:18.16.0
WORKDIR /app
COPY . .
RUN yarn
RUN yarn build
CMD yarn start-prod